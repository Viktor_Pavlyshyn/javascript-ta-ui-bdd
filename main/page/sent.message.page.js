class SentMessagePage {

    get sentMessage (){ return $("//tr[1][ancestor::div[@class='ae4 UI']]//span[@class='y2']") }

    get allCheckbox (){ return $("//div[@gh='tm']//span[@class='T-Jo J-J5-Ji']") }

    get deleteMsg (){ return $("//div[@gh='tm']//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs mA'][descendant::div[@class='asa' and div[@class='ar9 T-I-J3 J-J5-Ji']]]") }

    get refSendInClearAreaMsg (){ return $("//span[parent::td[@class='TC']]") }
}

export default new SentMessagePage();