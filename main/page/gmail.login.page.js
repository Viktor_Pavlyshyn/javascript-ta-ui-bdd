class GmailLoginPage {
    
    get setLogin (){ return $("//input[@id='identifierId']") }

    get submitButtonLogin () { return $("//button[@jsname='LgbsSe']") }

    get setPassword () { return $("//input[@name='password']") }

    get submitButtonPassword (){ return $("//button[@jscontroller='soHxf'][ancestor::div[@class='qhFLie']]") }
}

export default new GmailLoginPage();