Feature: Gmail send message.

Background: 
    Given open browser on url "https://mail.google.com/mail"

Scenario Outline: Verify send message.
    When login using login as "<login>" and password as "<password>"
    Then click 'Compose' button
    Then fill 'To' as "<recipient>", 'Subject' as "<topic>" and 'message' fields and click 'sent' button
    Then click 'sent' tab
    Then verify that message is in 'sent' folder
    Then remove message from the 'sent' folder

Examples:
    | login                 | password   | recipient            | topic      |
    | test5selen@gmail.com  | root5test  | test7selen@gmail.com | test gmail |