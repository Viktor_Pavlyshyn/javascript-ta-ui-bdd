import { Given, When, Then, setDefaultTimeout } from 'cucumber';
import { assert }  from'chai';
import GmailLoginBO  from'../../main/businessobject/gmail.login.bo';
import GmailHomeBO  from'../../main/businessobject/gmail.home.bo';
import GmailSentMessageBO  from'../../main/businessobject/gmail.sent.message.bo';

setDefaultTimeout(60000);
const message = Math.floor(Math.random() * (1000000 - 1 + 1)) + 1;

Given ("open browser on url {string}", (url) => {
   return browser.url(url);
});

When ("login using login as {string} and password as {string}", (login, password) => {

    GmailLoginBO.loginToGmail(login, password);
});

Then ("click 'Compose' button", () => {

    GmailHomeBO.clickCompose();
});

Then ("fill 'To' as {string}, 'Subject' as {string} and 'message' fields and click 'sent' button", (recipient, topic) => {

    GmailHomeBO.writeAndSendMessage(recipient, topic, message);
});

Then ("click 'sent' tab", () => {

    GmailHomeBO.navigateToSentLetter();
});

Then ("verify that message is in 'sent' folder", () => {

    assert.isTrue(GmailSentMessageBO.getSentMsgText().includes(message), 'Wrong text input.');
});

Then ("remove message from the 'sent' folder", () => {

    GmailSentMessageBO.deleteFirstLetter();
});